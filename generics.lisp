;; ;;; Declarations for inlined-generic-function

;; (defgeneric add-node
;;   (BUFFER NODE &OPTIONAL X Y (Z) REVIVE-P)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Add the mode NODE to the BUFFER.
;; Optionally set the location with X,Y,Z."))

;; (defgeneric aim
;;   (NODE HEADING)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Set the NODE's current heading to HEADING."))

;; (defgeneric aim-at
;;   (NODE OTHER-NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Set the NODE's heading to aim at the OTHER-NODE."))

;; (defgeneric backward
;;   (NODE DISTANCE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Move NODE backward DISTANCE units along the aim direction."))

;; (defgeneric bounding-box
;;   (NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Return as multiple values the coordinates of the bounding box for
;; NODE. These are given in the order (TOP LEFT RIGHT BOTTOM)."))

;; (defgeneric bounding-box*
;;   (NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric save-location
;;   (NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric restore-location
;;   (NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric center-point
;;   (NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Return as multiple values the coordinates of the NODE's center point."))

;; (defgeneric find-identifier
;;   (NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function))

;; (defgeneric collide
;;   (NODE OTHER-NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "This method is invoked when NODE collides with OTHER-NODE."))

;; (defgeneric colliding-with-p
;;   (NODE OTHER-NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Return non-nil if NODE's bounding box touches OTHER-NODE's bounding
;; box."))

;; (defgeneric colliding-with-rectangle-p
;;   (NODE TOP LEFT WIDTH HEIGHT)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Return non-nil if NODE is colliding with the given rectangle."))

;; (defgeneric destroy
;;   (NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Destroy this NODE and remove it from any buffers."))

;; (defgeneric direction-to
;;   (NODE OTHER-NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Returns the approximate keyword compass direction between NODE and OTHER-NODE."))

;; (defgeneric distance-between
;;   (NODE OTHER-NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Returns the distance between NODE and OTHER-NODE's center points."))

;; (defgeneric duplicate
;;   (NODE &REST INITARGS)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Make a copy of this NODE."))

;; (defgeneric forward
;;   (NODE DISTANCE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Move the NODE forward along its current heading for DISTANCE units."))

;; (defgeneric handle-collision
;;   (NODE OTHER-NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Wraparound for collision handling. You shouldn't need to use this
;; explicitly."))

;; (defgeneric contains-node-p
;;   (BUFFER NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Return non-nil if BUFFER contains NODE."))

;; (defgeneric heading-between
;;   (NODE OTHER-NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Return the angle (in radians) of the ray from NODE to OTHER-NODE."))

;; (defgeneric move
;;   (NODE HEADING DISTANCE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Move the NODE toward HEADING by DISTANCE units."))

;; (defgeneric move-to
;;   (NODE X Y &OPTIONAL Z)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Move the NODE to the point X,Y,Z."))

;; (defgeneric move-toward
;;   (NODE DIRECTION &OPTIONAL (STEPS))
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Move the node STEPS units in compass direction
;; DIRECTION."))

;; (defgeneric quadtree-delete
;;     (NODE TREE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   )

;; (defgeneric quadtree-insert
;;   (NODE TREE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   )

;; (defgeneric remove-node
;;   (BUFFER NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Remove the object NODE from BUFFER."))

;; (defgeneric resize
;;   (NODE WIDTH HEIGHT)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Resize the NODE to be WIDTH by HEIGHT units."))

;; (defgeneric step-toward-heading
;;   (NODE HEADING &OPTIONAL (DISTANCE))
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Return as multiple values the point DISTANCE units
;; at angle HEADING away from the center of NODE. "))

;; (defgeneric turn-left
;;   (NODE RADIANS)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Increase heading by RADIANS."))

;; (defgeneric turn-right
;;   (NODE RADIANS)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Decrease heading by RADIANS."))

;; (defgeneric update
;;   (NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Update the node's game logic for one frame."))

;; (defgeneric x
;;   (NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Return the current x-coordinate of the NODE."))

;; (defgeneric y
;;   (NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Return the current y-coordinate of the NODE."))

;; (defgeneric z
;;   (NODE)
;;   (:generic-function-class inlined-generic-function:inlined-generic-function)
;;   (:documentation "Return the current z-coordinate of the NODE."))
