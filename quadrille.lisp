;;; quadrille.lisp --- quadtree for detecting collisions between AABB's

;; Copyright (C) 2011-2016  David O'Toole <dto@xelf.me>
;; Please see also the included file LICENSE.

;; Licensed material: 

;; The "memoization" functions in this file are originally written by
;; Peter Norvig for his book "Paradigms of Artificial Intelligence
;; Programming". The modified versions are redistributed here under
;; the terms of the General Public License as given above.

;; You can find more information on Norvig's book at his website:

;; http://www.norvig.com/paip.html

;; The full license for the PAIP code, which governs the terms of
;; said redistribution under the GPL, can be found at norvig.com:

;; http://www.norvig.com/license.html

(in-package :quadrille)

(defun cfloat (f) (coerce f 'single-float))

;;; Vertex arrays

(defvar *vertex-array* nil)

(defun initialize-vertex-arrays ()
  (setf *vertex-array* (cffi:foreign-alloc :float :count 16))
  (setf (cffi:mem-aref *vertex-array* :float 0) 0.0)
  (setf (cffi:mem-aref *vertex-array* :float 1) 1.0)
  (setf (cffi:mem-aref *vertex-array* :float 4) 1.0)
  (setf (cffi:mem-aref *vertex-array* :float 5) 1.0)
  (setf (cffi:mem-aref *vertex-array* :float 8) 1.0)
  (setf (cffi:mem-aref *vertex-array* :float 9) 0.0)
  (setf (cffi:mem-aref *vertex-array* :float 12) 0.0)
  (setf (cffi:mem-aref *vertex-array* :float 13) 0.0))

(defun update-vertex-arrays (x y width height)
  (let ;; ((x1 (cfloat x))
	((x2 (+ x width))
	;; (y1 (cfloat y))
	 (y2 (+ y height)))
      ;;
      (setf (cffi:mem-aref *vertex-array* :float 2) x)
      (setf (cffi:mem-aref *vertex-array* :float 3) y2)
      ;;
      (setf (cffi:mem-aref *vertex-array* :float 6) x2)
      (setf (cffi:mem-aref *vertex-array* :float 7) y2)
      ;;
      (setf (cffi:mem-aref *vertex-array* :float 10) x2)
      (setf (cffi:mem-aref *vertex-array* :float 11) y)
      ;;
      (setf (cffi:mem-aref *vertex-array* :float 14) x)
      (setf (cffi:mem-aref *vertex-array* :float 15) y)))

;;; Blocky compatibility functions

(defun field-value (field object) (slot-value object field))
(defun set-field-value (field object value) (setf (slot-value object field) value))
(defsetf field-value set-field-value)

;;; Memoization facility

(defmacro defun-memo (name args memo-args &body body)
  "Define a memoized function named NAME.
ARGS is the lambda list giving the memoized function's arguments.
MEMO-ARGS is a list with optional keyword arguments for the
memoization process: :KEY, :VALIDATOR, and :TEST."
  `(memoize (defun ,name ,args . ,body) ,@memo-args))

(defun memo (fn &key (key #'first) (test #'eql) validator name)
  "Return a memo-function of fn."
;;  (declare (optimize (speed 3) (safety 2)))
  (let ((table (make-hash-table :test test)))
    (setf (get name 'memo) table)
    #'(lambda (&rest args)
        (let ((k (funcall key args)))
          (multiple-value-bind (val found-p)
              (gethash k table)
            (if found-p 
		val
		;; only cache if value is valid
		(let ((candidate-value (apply fn args)))
		  (prog1 candidate-value
		    (when (or (null validator)
			      (funcall validator candidate-value))
		      (setf (gethash k table) candidate-value))))))))))

(defun memoize (fn-name &key (key #'first) (test #'eql) validator)
  "Replace fn-name's global definition with a memoized version."
  (clear-memoize fn-name)
  (setf (symbol-function fn-name)
        (memo (symbol-function fn-name)
              :name fn-name :key key :test test :validator validator)))

(defun clear-memoize (fn-name)
  "Clear the hash table from a memo function."
  (let ((table (get fn-name 'memo)))
    (when table (clrhash table))))

(defun get-memo-table (fn-name)
  (get fn-name 'memo))

(defun-memo pretty-string (thing)
    (:key #'first :test 'equal :validator #'identity)
  (let ((name (etypecase thing
		(symbol (symbol-name thing))
		(string thing))))
    (coerce 
     (substitute #\Space #\- 
		 (string-downcase 
		  (string-trim " " name)))
     'simple-string)))

(defun-memo ugly-symbol (string)
    (:key #'first :test 'equal :validator #'identity)
  (intern 
   (string-upcase
    (substitute #\- #\Space 
		(string-trim " " string)))))

(defun-memo make-keyword (S) (:test 'eq) 
  "Make the symbol or string S into a keyword symbol."
  (etypecase S
    (string (intern (string-upcase S) :keyword))
    (symbol (intern (symbol-name S) :keyword))))

(defun make-non-keyword (S)
  "Make the symbol or string S into a non-keyword symbol."
  (etypecase S
    (symbol (intern (symbol-name S)))
    (string (intern (string-upcase S)))))

;;; Hooks

(defun add-to-list (list element)
  "Add the item ELEMENT to the list LIST."
  (assert (and (symbolp list)
	       (not (null list))))
  (setf (symbol-value list)
	(append (symbol-value list)
		(list element))))
	 
(defun add-hook (hook func)
  "Hooks are special variables whose names are of the form
`*foo-hook*' and whose values are lists of functions taking no
arguments. The functions of a given hook are all invoked (in list
order) whenever the hook is run with `run-hook'.

This function arranges for FUNC to be invoked whenever HOOK is triggered with
`run-hook'. The function should have no arguments."
  (pushnew func (symbol-value hook)))

(defun remove-hook (hook func)
  "Stop calling FUNC whenever HOOK is triggered."
  (setf (symbol-value hook)
	(delete func (symbol-value hook))))

(defun run-hook (hook)
  "Call all the functions in HOOK, in list order."
  (dolist (func (symbol-value hook))
    (funcall func)))

;;; A notion of time

(defvar *updates* 0)
(defparameter *unit* 20)
(defun units (n) (* n *unit*))
(defconstant +60fps+ 60)
(defconstant +30fps+ 30)
(defparameter *time-base* +60fps+)
(defconstant +seconds-per-minute+ 60)
(defun seconds (n) (* *time-base* n))
(defun minutes (n) (* (seconds +seconds-per-minute+) n))

;;; UUID object dictionary

(defun make-uuid ()
  (coerce 
   (subseq
    (format nil "~7,'0X-~A-~7,'0X" (random 99999) *updates* (random 99999))
    0 16)
   'simple-string))

(defvar *database* nil)

(defun initialize-database ()
  (setf *database* 
	(make-hash-table :test 'equal :size 8192)))

(defun add-object-to-database (object)
  (when (null *database*)
    (initialize-database))
  (setf (gethash 
	 (the simple-string (uuid object))
	 *database*)
	object))

(defun remove-object-from-database (object)
  (let ((total (hash-table-count *database*)))
    (assert (hash-table-p *database*))
    (assert (plusp total))
    (remhash 
     (the simple-string (uuid object))
     *database*)))

(defun find-object-by-uuid (uuid &optional noerror)
  (or (gethash (the simple-string uuid) *database*)
      (unless noerror
	(error "Cannot find object for uuid ~S" uuid))))
		  
;;; Finding any object by proto-name or UUID

(defun find-object (thing &optional no-error) 
  (if (stringp thing)
      (find-object-by-uuid thing :noerror)
      thing))

;; (defun find-object (thing &optional no-error) 
;;   (when (not (null thing))
;;     (let ((result 
;; 	    (etypecase thing
;; 	      (string (find-object-by-uuid thing :noerror))
;; 	      (quadrille thing))))
;;       (prog1 result
;; 	(unless no-error
;; 	  (when (null result)
;; 	    (error "Cannot find object: ~S" thing)))))))
      
(defun object-eq (a b)
  (when (and a b)
    (eq (find-object a)
	(find-object b))))

;;; Math functions

(defun bounding-box-contains (box0 box1)
  (destructuring-bind (top0 left0 right0 bottom0) box0
    (destructuring-bind (top1 left1 right1 bottom1) box1
      (declare (single-float top0 left0 right0 bottom0 top1 left1 right1 bottom1) 
	       (optimize (speed 3)))
      (and (<= top0 top1)
	   (<= left0 left1)
	   (>= right0 right1)
	   (>= bottom0 bottom1)))))

(defun distance (x1 y1 x2 y2) 
  "Compute the distance between the points X1,Y1 and X2,Y2."
  (let ((delta-x (- x2 x1))
	(delta-y (- y2 y1)))
    (sqrt (+ (* delta-x delta-x) (* delta-y delta-y)))))

(defvar *directions* (list :right :upright :up :upleft :left :downleft :down :downright)
  "List of keywords representing the eight compass directions.")

(defvar *opposites* (list :up :down
			  :down :up
			  :right :left
			  :left :right
			  :upright :downleft
			  :downleft :upright
			  :downright :upleft
			  :upleft :downright
			  :here :here)
  "Property list mapping direction keywords to their 180-degree
opposites.")

(defparameter *left-turn* 
  '(:up :upleft
    :upleft :left
    :left :downleft
    :downleft :down
    :down :downright
    :downright :right
    :right :upright
    :upright :up))

(defparameter *right-turn*
  '(:up :upright
    :upright :right
    :right :downright
    :downright :down
    :down :downleft
    :downleft :left
    :left :upleft
    :upleft :up))

(defun left-turn (direction)
  (getf *left-turn* direction))

(defun right-turn (direction)
  (getf *right-turn* direction))

(defun opposite-direction (direction)
  "Return the direction keyword that is the opposite direction from
DIRECTION."
  (getf *opposites* direction))

(defun random-direction ()
  (nth (random (length *directions*))
       *directions*))

(defun direction-degrees (direction)
  "Return the angle (in degrees) which DIRECTION points."
  (ecase direction
    (:up -90)
    (:down 90)
    (:right 0)
    (:left -180)
    (:upright -45)
    (:upleft -135) 
    (:downright 45)
    (:downleft 135)
    (:here 0)))

(defun radian-angle (degrees)
  "Convert DEGREES to radians."
  (* degrees (cfloat (/ pi 180))))

(defun heading-degrees (radians)
  (* radians (cfloat (/ 180 pi))))

(defun direction-heading (direction)
  "Return the angle (in radians) of the keyword DIRECTION."
  (radian-angle (direction-degrees direction)))
   
(defun heading-direction (heading)
  (flet ((pointing (direction)
  	   (when (<= (abs (- heading
  			    (direction-heading direction)))
		     (/ pi 7))
  	     direction)))
    (some #'pointing *directions*)))

(defun step-in-direction (x y direction &optional (n 1))
  "Return the point X Y moved by n squares in DIRECTION."
  (ecase direction
    (:here (values x y))
    (:up (values x (- y n)))
    (:down (values x (+ y n)))
    (:right  (values (+ x n) y))
    (:left  (values (- x n) y))
    (:upright (values (+ x n) (- y n)))
    (:upleft (values (- x n) (- y n)))
    (:downright (values (+ x n) (+ y n)))
    (:downleft (values (- x n) (+ y n)))))

(defun find-heading (x0 y0 x1 y1)
  "Return the angle in radians of the ray from the point X0,Y0 to the
point X1,Y1."
  (atan (- y1 y0) 
	(- x1 x0)))

(defun opposite-heading (heading)
  "Return the heading angle opposite to HEADING."
  (mod (+ pi heading)
       (* 2 pi)))
;;  my previous incorrect code for this is memorialized here as my punishment!
;;  (- pi heading))

(defun find-direction (x1 y1 x2 y2)
  "Return the heading (in radians) of the ray from Y1,X1 to Y2,X2."
  (if (or (some #'null (list y1 x1 y2 x2))
	  (and (= y1 y2) (= x1 x2)))
      :here
      (if (< y1 y2) ; definitely to the down
	  (if (< x1 x2)
	      :downright
	      (if (> x1 x2)
		  :downleft
		  :down))
	  (if (> y1 y2) ;; definitely to the up
	      (if (< x1 x2)
		  :upright
		  (if (> x1 x2)
		      :upleft
		      :up))
	      ;; rows are equal; it's either right or left
	      (if (< x1 x2)
		  :right
		  :left)))))

(defun within-extents (x y x0 y0 x1 y1)
  (and (>= x x0) 
       (<= x x1)
       (>= y y0)
       (<= y y1)))

(defvar *quadtree* nil)

(defmacro with-quadtree (quadtree &body body)
  "Evaluate BODY forms with *QUADTREE* bound to QUADTREE."
  `(let* ((*quadtree* ,quadtree))
       ,@body))

(defvar *quadtree-depth* 0)
(defparameter *default-quadtree-depth* 6) 
 
(defclass quadtree ()
  ((objects :initform nil :accessor quadtree-objects :initarg :objects)
   (level :initform nil :accessor quadtree-level :initarg :level)
   (top :initform nil :accessor quadtree-top :initarg :top)
   (left :initform nil :accessor quadtree-left :initarg :left)
   (right :initform nil :accessor quadtree-right :initarg :right)
   (bottom :initform nil :accessor quadtree-bottom :initarg :bottom)
   (southwest :initform nil :accessor quadtree-southwest :initarg :southwest)
   (northeast :initform nil :accessor quadtree-northeast :initarg :northeast)
   (northwest :initform nil :accessor quadtree-northwest :initarg :northwest)
   (southeast :initform nil :accessor quadtree-southeast :initarg :southeast)))

(defclass quadrille () 
  ((quadtree-node :initform nil :initarg :quadtree-node :accessor quadtree-node)
   (collision-type :initform t :initarg :collision-type :accessor collision-type)
   (uuid :initform nil :accessor uuid :initarg :uuid)
   (heading :initform 0.0 :accessor heading)
   (width :initform 32 :accessor width)
   (height :initform 32 :accessor height)
   (x :initform (cfloat 0) :accessor x)
   (y :initform (cfloat 0) :accessor y)
   (z :initform (cfloat 0) :accessor z)
   (last-x :initform nil :accessor last-x)
   (last-y :initform nil :accessor last-y)
   (last-z :initform nil :accessor last-z)))

(defmethod resize ((quadrille quadrille) width height)
  (quadtree-delete-maybe quadrille (quadtree-node quadrille))
  (setf (height quadrille) height)
  (setf (width quadrille) width)
  (quadtree-insert-maybe quadrille (quadtree-node quadrille))
  nil)

(defvar *buffer* nil)
(defun current-buffer () *buffer*)

(defun switch-to-buffer (buffer)
  (setf *buffer* buffer))

(defmacro with-buffer (buffer &rest body)
  "Evaluate the BODY forms in the BUFFER."
  `(let* ((*buffer* ,buffer))
     ,@body))

(defclass qbuffer (quadrille)
  ((objects :accessor objects :initform nil)
   (quadtree :initform nil :accessor quadtree)
   (quadtree-depth :initform 4 :accessor quadtree-depth)))

(defmethod initialize-instance :after ((qbuffer qbuffer) &key)
  (setf (objects qbuffer)
	(make-hash-table :test 'equal)))

(defmethod contains-node-p ((qbuffer qbuffer) (quadrille quadrille))
  (gethash (uuid quadrille) (objects qbuffer)))

(defmethod add-node ((buffer qbuffer) (node quadrille) &optional x y (z 0) revive-p)
  (declare (ignore z) (ignore revive-p))
  (with-buffer buffer
    (with-quadtree (quadtree buffer)
      (let ((uuid (uuid node)))
	  (declare (simple-string uuid))
	  (setf (gethash uuid (objects buffer))
		(quadrille:find-identifier node))
	  (when (and (numberp x) (numberp y))
	    (move-to node x y))))))
      
(defmethod remove-node ((buffer qbuffer) (node quadrille))
  (with-buffer buffer
    (quadtree-delete-maybe node (quadtree-node node))
    (remhash (the simple-string (uuid node))
	     (objects buffer))))

(defmethod get-nodes ((buffer qbuffer)) 
  (loop for object being the hash-keys in (objects buffer)
     when (find-object object :no-error)
     collect (find-object object)))

(defmethod install-quadtree ((buffer qbuffer))
  (let ((box (multiple-value-list (bounding-box buffer))))
    (with-slots (quadtree quadtree-depth) buffer
      (setf quadtree (build-quadtree box (or quadtree-depth 
					     *default-quadtree-depth*)))
      (assert quadtree)
      (let ((objects (get-nodes buffer)))
	(when objects
	  (quadtree-fill objects quadtree))))))

(defmethod resize ((buffer qbuffer) new-width new-height)
  (assert (and (plusp new-height)
	       (plusp new-width)))
  (with-slots (height width quadtree) buffer
    (setf height new-height)
    (setf width new-width)
    (when quadtree
      (install-quadtree buffer))))

(defun find-bounding-box (nodes)
  "Return as multiple values the minimal bounding box 
containing the NODES."
  ;; calculate the bounding box of a list of nodes
  (labels ((left (thing) (slot-value thing 'x))
	   (right (thing) (+ (slot-value thing 'x)
			     (slot-value thing 'width)))
	   (top (thing) (slot-value thing 'y))
	   (bottom (thing) (+ (slot-value thing 'y)
			      (slot-value thing 'height))))
    ;; let's find the bounding box.
    (values (reduce #'min (mapcar #'top nodes))
	    (reduce #'min (mapcar #'left nodes))
	    (reduce #'max (mapcar #'right nodes))
	    (reduce #'max (mapcar #'bottom nodes)))))

(defmacro do-nodes (spec &body body)
  "Iterate over the nodes in BUFFER, binding VAR to each node and
evaluating the forms in BODY for each. SPEC is of the form (VAR
BUFFER)."
  (let ((node (gensym)))
    (destructuring-bind (var buffer) spec 
      `(loop for ,node being the hash-values in (field-value 'objects ,buffer)
	     do (let ((,var (find-object ,node)))
		  ,@body)))))

(defmethod register-uuid ((quadrille quadrille))
  (add-object-to-database quadrille))

(defmethod initialize-instance :after ((quadrille quadrille) &key)
  (when (null (uuid quadrille))
    (setf (uuid quadrille) (make-uuid))
    (register-uuid quadrille)))

(defmethod remove-node-maybe ((buffer qbuffer) (node quadrille))
  (with-buffer buffer
    (when (contains-node-p buffer node)
      (remove-node buffer node))))

(defmethod destroy ((quadrille quadrille))
  (with-slots (quadtree-node uuid) quadrille
    (quadtree-delete-maybe quadrille quadtree-node)
    (remove-node-maybe (current-buffer) quadrille)
    (setf quadtree-node nil)
    (remove-object-from-database quadrille)
    (prog1 t
      (assert (not (find-object uuid :no-error))))))

(defmethod save-location ((quadrille quadrille))
  (with-slots (x y z last-x last-y last-z) quadrille
    (setf last-x x
	  last-y y
	  last-z z)))

(defmethod clear-saved-location ((quadrille quadrille))
  (with-slots (last-x last-y last-z) quadrille
    (setf last-x nil last-y nil last-z nil)))

(defmethod clear-buffer-data ((self quadrille))
  (clear-saved-location self)
  (setf (field-value 'quadtree-node self) nil))

(defmethod restore-location ((quadrille quadrille))
  (with-slots (x y z last-x last-y last-z quadtree-node) quadrille
    (when last-x
      (quadtree-delete-maybe quadrille quadtree-node)
      (setf x last-x
	    y last-y
	    z last-z)
      (quadtree-insert-maybe quadrille quadtree-node))))
  
(defmethod move-to ((node quadrille) x0 y0 &optional z0)
  (with-slots (x y z) node
    (setf x x0 y y0)
    (when z (setf z z0)))
  (update-bounding-box node *quadtree*)
  nil)

(defgeneric bounding-box (object)
  (:documentation 
"Return the bounding-box of this OBJECT as multiple values.
The proper VALUES ordering is (TOP LEFT RIGHT BOTTOM), which could
also be written (Y X (+ X WIDTH) (+ Y HEIGHT)) if more convenient."))

(defmethod bounding-box ((quadrille quadrille))
  "Return this object's bounding box as multiple values.
The order is (TOP LEFT RIGHT BOTTOM)."
  (with-slots (x y width height) quadrille
    (values 
     (cfloat y)
     (cfloat x)
     (cfloat (+ x width))
     (cfloat (+ y height)))))

(defmethod center-point ((quadrille quadrille))
  (multiple-value-bind (top left right bottom)
      (the (values float float float float) (bounding-box quadrille))
    (let ((half (cfloat 0.5)))
      (declare (single-float half top left right bottom) (optimize (speed 3)))
      (values (* half (+ left right))
	      (* half (+ top bottom))))))

(defmethod bounding-box* ((quadrille quadrille))
  (multiple-value-bind (top left right bottom) (bounding-box quadrille)
    (values left top (- right left) (- bottom top))))

(defgeneric find-identifier (object)
  (:documentation
"Return an opaque identifier that is #'EQ across calls.
The default is to simply return the object. Customizing this is not
currently documented."))

(defmethod update ((quadrille quadrille)) nil)

(defmethod find-identifier ((quadrille quadrille)) 
  (uuid quadrille))

(defgeneric collide (this that)
  (:documentation
"Trigger defined collision methods for when THIS collides with THAT.
If a collision method is defined as (COLLIDE CLASS-1 CLASS-2), then
this COLLIDE will trigger when QUADTREE-COLLIDE is called on instances
of CLASS-1 that are colliding with instances of CLASS-2. 

If (COLLIDE CLASS-2 CLASS-1) is also defined, it will be triggered
only when QUADTREE-COLLIDE is called on colliding instances of
CLASS-2.

If you always want both orderings of the class pair's COLLIDE to be
triggered, then you must call QUADTREE-COLLIDE on every object in the
scene."))

(defmethod collide ((this quadrille) (that quadrille)) nil)

(defmethod handle-collision ((this quadrille) (that quadrille)) 
  (collide this that))

(defun point-in-rectangle-p (x y width height o-top o-left o-width o-height)
  (declare (single-float x y width height o-top o-left o-width o-height)
	   (optimize (speed 3)))
  (not (or 
	;; is the top below the other bottom?
	(<= (+ o-top o-height) y)
	;; is bottom above other top?
	(<= (+ y height) o-top)
	;; is right to left of other left?
	(<= (+ x width) o-left)
	;; is left to right of other right?
	(<= (+ o-left o-width) x))))

(defmethod colliding-with-rectangle-p ((self quadrille) o-top o-left o-width o-height)
  ;; you must pass arguments in Y X order since this is TOP then LEFT
  (multiple-value-bind (x y width height) (bounding-box* self)
    (point-in-rectangle-p (cfloat x) (cfloat y) (cfloat width) (cfloat height) 
			  (cfloat o-top) (cfloat o-left) (cfloat o-width) (cfloat o-height))))

(defun colliding-with-bounding-box-p (self top left right bottom)
  ;; you must pass arguments in Y X order since this is TOP then LEFT
  (multiple-value-bind (x y width height) (bounding-box* self)
    (when (and width height)
      (point-in-rectangle-p (cfloat x) (cfloat y) (cfloat width) (cfloat height)
			    top left (- right left) (- bottom top)))))

(defgeneric colliding-with-p (this that)
  (:documentation 
"Return non-nil when bounding boxes of THIS and THAT are colliding."))

(defmethod colliding-with-p ((self quadrille) (thing quadrille))
  (multiple-value-bind (top left right bottom) 
      (bounding-box thing)
    (colliding-with-bounding-box-p self top left right bottom)))

;;; Finding identified quadrille objects

(defvar *identifier-search-function* #'find-object
"Value must be a function accepting an opaque ID and returning the
corresponding object. Used by SEARCH-IDENTIFIER.")

(defun search-identifier (x)
  (funcall *identifier-search-function* x))

;;; Quadtree operations

(defmethod leafp ((node quadtree))
  ;; testing any quadrant will suffice
  (null (quadtree-southwest node)))

(defmethod quadtree-contains ((quadtree quadtree) top left right bottom)
  (declare (single-float top left right bottom) (optimize (speed 3)))
  (and (<= (the single-float (quadtree-top quadtree)) top)
       (<= (the single-float (quadtree-left quadtree)) left)
       (>= (the single-float (quadtree-right quadtree)) right)
       (>= (the single-float (quadtree-bottom quadtree)) bottom)))

(defun valid-bounding-box-p (box)
  (and (listp box)
       (= 4 (length box))
       (destructuring-bind (top left right bottom) box
	 (and (<= left right) (<= top bottom)))))

(defun northeast-quadrant (bounding-box)
  (destructuring-bind (top left right bottom) bounding-box
    (list top (cfloat (/ (+ left right) 2))
	  right (cfloat (/ (+ top bottom) 2)))))

(defun southeast-quadrant (bounding-box)
  (destructuring-bind (top left right bottom) bounding-box
    (list (cfloat (/ (+ top bottom) 2)) (cfloat (/ (+ left right) 2))
	  right bottom)))

(defun northwest-quadrant (bounding-box)
  (destructuring-bind (top left right bottom) bounding-box
    (list top left
	  (cfloat (/ (+ left right) 2)) (cfloat (/ (+ top bottom) 2)))))

(defun southwest-quadrant (bounding-box)
  (destructuring-bind (top left right bottom) bounding-box
    (list (cfloat (/ (+ top bottom) 2)) left
	  (cfloat (/ (+ left right) 2)) bottom)))

(defmethod quadtree-process ((node quadtree) top left right bottom processor)
  (when (quadtree-contains node top left right bottom)
    (when (not (leafp node))
      (quadtree-process (quadtree-northwest node) top left right bottom processor)
      (quadtree-process (quadtree-northeast node) top left right bottom processor)
      (quadtree-process (quadtree-southwest node) top left right bottom processor)
      (quadtree-process (quadtree-southeast node) top left right bottom processor))
    (funcall processor node)))

(defun build-quadtree (bounding-box0 &optional (depth *default-quadtree-depth*))
  (let ((bounding-box (mapcar #'cfloat bounding-box0)))
    (destructuring-bind (top left right bottom) bounding-box
      (decf depth)
      (if (zerop depth)
	  (make-instance 'quadtree :top top :left left :right right :bottom bottom)
	  (make-instance 'quadtree :top top :left left :right right :bottom bottom
			 :northwest (build-quadtree (northwest-quadrant bounding-box) depth)
			 :northeast (build-quadtree (northeast-quadrant bounding-box) depth)
			 :southwest (build-quadtree (southwest-quadrant bounding-box) depth)
			 :southeast (build-quadtree (southeast-quadrant bounding-box) depth))))))

(defun quadtree-search (top left right bottom node)
  "Return the smallest quadrant enclosing TOP LEFT RIGHT BOTTOM at or below
NODE, if any."
  (when (quadtree-contains node top left right bottom)
    ;; ok, it's in the overall bounding-box.
    (if (leafp node)
	;; there aren't any quadrants to search. stop here.
	node
	(or
	 ;; search the quadrants.
	 (or (quadtree-search top left right bottom (quadtree-northwest node))
	     (quadtree-search top left right bottom (quadtree-northeast node))
	     (quadtree-search top left right bottom (quadtree-southwest node))
	     (quadtree-search top left right bottom (quadtree-southeast node)))
	 ;; none of them are suitable. stay here
	 node))))

(defgeneric quadtree-insert (object tree)
  (:documentation
"Insert the object OBJECT into the quadtree TREE."))

(defmethod quadtree-insert ((object quadrille) (tree quadtree))
  (let ((node0 
	  (multiple-value-bind (top left right bottom) (bounding-box object)
	    (quadtree-search top left right bottom tree))))
    (let ((node (or node0 tree)))
      (pushnew (find-identifier object)
	       (quadtree-objects node)
	       :test 'eq)
      ;; save pointer to node so we can avoid searching when it's time
      ;; to delete (i.e. move) the object later.
      (setf (quadtree-node object) node))))

(defgeneric quadtree-delete (object tree)
  (:documentation
"Delete the object OBJECT from the quadtree TREE."))

(defmethod quadtree-delete ((object quadrille) (tree quadtree))
  ;; grab the cached quadtree node
  (let ((node (or (quadtree-node object) tree)))
    (setf (quadtree-objects node)
	  (delete (find-identifier object) (quadtree-objects node) :test 'eq))
    (setf (quadtree-node object) nil)))

(defmethod quadtree-insert-maybe ((object quadrille) tree)
  (when tree
    (quadtree-insert object tree)))

(defmethod quadtree-delete-maybe ((object quadrille) tree)
  (when (and tree (quadtree-node object))
    (quadtree-delete object tree)))

(defgeneric update-bounding-box (object quadtree)
  (:documentation 
"Update the OBJECT's new bounding box and position in QUADTREE."))

(defmethod update-bounding-box ((object quadrille) tree)
  (with-quadtree tree
    (quadtree-delete-maybe object tree)
    (quadtree-insert-maybe object tree)))

(defmethod quadtree-map-collisions ((tree quadtree) top left right bottom processor)
  (quadtree-process tree
   top left right bottom
   #'(lambda (node)
       (let (garbage)
	 (dolist (object (quadtree-objects node))
	   (if (search-identifier object)
	       (when (colliding-with-bounding-box-p (search-identifier object) top left right bottom)
		 (funcall processor (search-identifier object)))
	       (push object garbage)))
	 (dolist (g garbage)
	   (setf (quadtree-objects node)
		 (delete g (quadtree-objects node) :test 'equal)))))))

(defgeneric quadtree-collide (object quadtree)
  (:documentation
"Detect and handle collisions of OBJECT with other objects within the
QUADTREE. The multimethod COLLIDE will be invoked on each pair of 
(OBJECT OTHER-OBJECT)"))

(defmethod quadtree-collide ((object quadrille) (tree quadtree))
  (multiple-value-bind (top left right bottom) (bounding-box object)
    (quadtree-map-collisions tree
     top left right bottom
     #'(lambda (thing)
	 (when (and (collision-type thing)
		    (colliding-with-p object thing)
		    (not (eq object thing)))
	   (with-quadtree tree
	     (handle-collision object thing)))))))

(defun quadtree-fill (set quadtree)
  "Insert the objects in SET (a list or hashtable) into QUADTREE."
  (let ((objects (etypecase set
		   (list set)
		   (hash-table (loop for object being the hash-keys in set collect object)))))
    (dolist (object objects)
      (setf (quadtree-node object) nil)
      (quadtree-insert object quadtree))))

(defun make-quadtree (x y width height &key objects (depth *default-quadtree-depth*))
  "Make a new quadtree with the given dimensions, OBJECTS, and DEPTH."
  (let ((quadtree (build-quadtree (list y x (+ x width) (+ y height)) depth)))
    (when objects
      (quadtree-fill objects quadtree))
    quadtree))

(defun collide-objects (objects quadtree)
  "Trigger all collisions for OBJECTS within QUADTREE."
  (dolist (object objects)
    (quadtree-collide object quadtree)))

(defun collide-objects* (objects quadtree)
  "Trigger all collisions for identified OBJECTS within QUADTREE."
  (dolist (id (mapcar #'find-identifier objects))
    ;; object might have been deleted by other collisions
    (when (search-identifier id)
      (quadtree-collide (search-identifier id) quadtree))))

(defvar *next-update-hook* nil)

(defmacro at-next-update (&body body)
  "Run the forms in BODY at the next game loop update."
  `(prog1 nil 
     (add-hook '*next-update-hook*
	       #'(lambda () ,@body))))

(defmethod run ((quadtree quadtree) objects)
  (with-quadtree quadtree
    (loop for uuid being the hash-keys in objects do
	 (let ((object? (search-identifier (gethash uuid objects))))
	   (if object?
	       (update object?)
	       (remhash (the simple-string uuid) objects))))
    ;; detect collisions
    (loop for uuid being the hash-keys in objects do
	 (let ((object? (search-identifier (gethash uuid objects))))
	   (when object?
	     (unless (eq :passive (slot-value object? 'collision-type))
	       (quadtree-collide object? quadtree)))))))

(defmethod update :before ((buffer qbuffer))
  (with-slots (quadtree objects) buffer
    (when (null quadtree)
      (install-quadtree buffer))
    ;; (assert quadtree)
    (quadrille:run quadtree objects)))

(defun update-system ()
  ;; (handler-case 
  ;;     (progn
  (incf *updates*)
  (run-hook '*next-update-hook*)
  (setf *next-update-hook* nil)
  (when *buffer* (update *buffer*)))
    ;; (floating-point-inexact (fpe)
    ;;   (error fpe))))

;;; Movement functions

(defmethod move-toward ((self quadrille) direction &optional (steps 1))
  (with-slots (x y) self
    (multiple-value-bind (x0 y0)
	(step-in-direction x y (or direction :up) (or steps 5))
      (move-to self x0 y0))))

(defmethod turn-left ((self quadrille) radians)
  (decf (heading self) radians))

(defmethod turn-right ((self quadrille) radians)
  (incf (heading self) radians))

(defun step-coordinates (x y heading &optional (distance 1))
  (values (+ x (* distance (cos heading)))
	  (+ y (* distance (sin heading)))))

(defmethod step-toward-heading ((self quadrille) heading &optional (distance 1))
  (multiple-value-bind (x y) (center-point self)
    (step-coordinates x y heading distance)))

(defmethod move ((self quadrille) heading distance)
  (with-slots (x y) self
    (multiple-value-bind (x0 y0) (step-coordinates x y heading distance)
      (move-to self x0 y0))))

(defmethod forward  ((self quadrille) distance)
  (move self (heading self) distance))

(defmethod backward  ((self quadrille) distance)
  (move self (opposite-heading (heading self)) distance))

(defmethod heading-to-thing2 ((self quadrille) thing)
  (multiple-value-bind (x1 y1) (center-point thing)
    (multiple-value-bind (x0 y0) (center-point self)
      (find-heading x0 y0 x1 y1))))

(defmethod heading-to-thing ((self quadrille) thing)
  (with-slots (x y) self 
    (multiple-value-bind (x0 y0) (center-point thing)
      (find-heading x y x0 y0))))

(defmethod direction-to ((self quadrille) thing)
  (with-slots (x y) self
    (with-slots (x0 y0) thing
      (find-direction x y x0 y0))))
  
(defmethod heading-between ((self quadrille) thing)
  (multiple-value-bind (x y) (center-point self)
    (multiple-value-bind (x0 y0) (center-point thing)
      (find-heading x y x0 y0))))

(defmethod aim-at  ((self quadrille) node)
  (setf (heading self) (heading-between self node)))

(defmethod aim  ((self quadrille) heading)
  (assert (numberp heading))
  (setf (heading self) heading))

(defmethod distance-between  ((self quadrille) (thing quadrille))
  (multiple-value-bind (x0 y0) (center-point self)
    (multiple-value-bind (x y) (center-point thing)
      (distance x0 y0 x y))))

;;; quadrille.lisp ends here
