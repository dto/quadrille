(asdf:defsystem #:quadrille
  :description "Quadrille performs efficient 2D collision detection on sets of
objects having axis-aligned bounding boxes."
  :author "David O'Toole <dto@xelf.me>"
  :maintainer "David O'Toole <dto@xelf.me>"
  :name "quadrille"
  :version "1.1"
  :license "MIT"
  :components ((:file "package")
	       (:file "generics" :depends-on ("package"))
	       (:file "quadrille" :depends-on ("generics"))))
